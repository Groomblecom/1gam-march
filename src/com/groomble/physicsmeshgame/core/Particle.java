package com.groomble.physicsmeshgame.core;

public class Particle {
	public Vec2 pos; // don't set directly from outside this class. ever.
	public Vec2 vel;
	Game mgame;
	protected boolean locked = false;
	private static Vec2 collisionHelper = new Vec2(); // kept about to avoid reallocating
	Particle(Vec2 tpos, Game tg) {
		pos = tpos;
		vel = new Vec2();
		mgame = tg;
	}
	void move() {
		if(locked)return;
		if(vel.mag() < 0.049)vel.zero();
		boolean done = false;
		double[] m = new double[2];
		m[0] = vel.y;// numerator first
		m[1] = vel.x;
		double k = pos.y-pos.x*m[0]/m[1];
		float hy, ly;
		if(vel.y > 0) {
			hy = pos.y+vel.y;
			ly = pos.y;
		}else{
			ly = pos.y+vel.y;
			hy = pos.y;
		}
		for(Wall w:mgame.walls) {
			if(m[0] == 0 && m[1] == 0)continue;
			double intersectionX = (w.getK()-k)/(m[0]/m[1]-w.getM()[0]/w.getM()[1]);
			if(Double.isInfinite(w.getM()[0]/w.getM()[1]))intersectionX=w.getEnd0X();
			if(Double.isInfinite(m[0]/m[1]))intersectionX=pos.x;
			double intersectionY = intersectionX*w.getM()[0]/w.getM()[1]+w.getK();
			if(Double.isInfinite(w.getM()[0]/w.getM()[1]))intersectionY=intersectionX*m[0]/m[1]+k;
//			if(Double.isInfinite(w.getM()[0]/w.getM()[1]))System.out.println("Fixing intersectionY :");
			if(intersectionX >= w.getEnd0X() && intersectionX <= w.getEnd1X()) { // is on line segment of wall
				if(intersectionY >= ly && intersectionY <= hy) { // and is on line segment of vel
					if(Double.isInfinite(w.getM()[0]/w.getM()[1])) {
						vel.x*=-1;
						done = true;
						break;
					}
					collisionHelper.set(vel);
					float[] diff = {(float) (intersectionX-pos.x), (float) (intersectionY-pos.y)};
					pos.x = (float) intersectionX;
					pos.y = (float) intersectionY;
					collisionHelper.normalize();
					double theta = java.lang.Math.acos(collisionHelper.dot(w.getNormal()));
					theta += java.lang.Math.PI/2;
					theta %= java.lang.Math.PI*2;
					collisionHelper.set(vel);
					collisionHelper.x-=diff[0];
					collisionHelper.y-=diff[1]; // collisionHelper is now 'rewound' to the point of intersection
					double cosTheta = java.lang.Math.cos(theta);
					double sinTheta = java.lang.Math.sin(theta);
					float tmag = vel.mag();
					vel.x = (float)cosTheta;
					vel.y = (float)sinTheta;
					vel.mult(-Wall.RESTITUTION*tmag);
					tmag = collisionHelper.mag();
					collisionHelper.x = (float)cosTheta;
					collisionHelper.y = (float)sinTheta;
					collisionHelper.mult(-tmag);
					pos.add(collisionHelper);
					done = true;
				}
			}
		}
		if(!done) {
			pos.add(vel);
			
		}
	}
	public boolean isLocked() {
		return locked;
	}
	public void setLocked(boolean locked) {
		this.locked = locked;
	}
}