package com.groomble.physicsmeshgame.core;

public class Wall {
	public static final float RESTITUTION = 0.7f;
	private Vec2[] ends = new Vec2[2];
	private double[] m; // numerator, denominator
	private double k;
	private boolean valid;
	private Vec2 normal;
	public Wall(Vec2 a, Vec2 b) { // If a.x > b.x, the vectors will be flipped.
		ends[0] = a;
		ends[1] = b;
		m = new double[2];
		normal = new Vec2();
		recalc();
	}
	void setEnds(Vec2[] tends) {
		ends = tends;
		valid = false;
		recalc();
	}
	public double[] getM() {
		if(valid) {
			return m;
		}
		recalc();
		return m;
	}
	public double getK() {
		if(valid) {
			return k;
		}
		recalc();
		return k;
	}
	public Vec2 getNormal() {
		if(valid) {
			return normal;
		}
		recalc();
		return normal;
	}
	private void recalc() {
		if(ends[0].x>ends[1].x) {
			Vec2 t = ends[0];
			ends[0] = ends[1];
			ends[1] = t;
		}
		m[0] = ends[0].y-ends[1].y;
		m[1] = ends[0].x-ends[1].x;
		k = ends[0].y-ends[0].x*m[0]/m[1];
		normal.set(ends[1].x-ends[0].x, ends[1].y-ends[0].y);
		float tx = normal.x;
		normal.x = normal.y;
		normal.y = tx;
		normal.normalize();
		valid = true;
	}
	public float getEnd0X() {
		return ends[0].x;
	}
	public float getEnd0Y() {
		return ends[0].y;
	}
	public float getEnd1X() {
		return ends[1].x;
	}
	public float getEnd1Y() {
		return ends[1].y;
	}
}
