package com.groomble.physicsmeshgame.core;

import java.util.ArrayList;

import com.groomble.physicsmeshgame.render.Renderer;

public class Game {
	private ArrayList<Particle> particles;
	private ArrayList<Force> forces;
	private ArrayList<ObjectiveBox> boxes;
	private ArrayList<ObjectiveParticle> objectiveparticles;
	public ArrayList<Wall> walls;
	
	private Renderer mrenderer;
	
	private Vec2 greater = new Vec2(); // state stuff (ignore)
	private Vec2 lesser = new Vec2();
	public int totalObjectives;
	public int completedCount;
	public int step;
	
	public Game() {
		setParticles(new ArrayList<Particle>());
		setForces(new ArrayList<Force>());
		setBoxes(new ArrayList<ObjectiveBox>());
		walls = new ArrayList<Wall>();
		objectiveparticles = new ArrayList<ObjectiveParticle>();
	}
	public void init(Renderer r) { // deferred initialization so loading screens, etc can be displayed
		System.out.println("Game initialized");
		mrenderer = r;
		r.begin(this);
	}
	public void step() {
		for(int i = 0; i < forces.size(); i++) {
			forces.get(i).apply();
		}
		for(Particle p:getParticles()) {
			p.move();
		}
		for(ObjectiveParticle p:objectiveparticles) {
			for(ObjectiveBox b:getBoxes()) {
				b.test(p);
			}
		}
		if(totalObjectives == completedCount) {
			mrenderer.win();
			mrenderer.setPaused(true);
		}
		step++;
	}
	public ArrayList<Particle> getParticles() {
		return particles;
	}
	public void setParticles(ArrayList<Particle> particles) {
		this.particles = particles;
	}
	public ArrayList<Force> getForces() {
		return forces;
	}
	public void setForces(ArrayList<Force> forces) {
		this.forces = forces;
	}
	public void cutSprings(Vec2 c, Vec2 v) {
		double m = (c.y-v.y)/(c.x-v.x);
		double b = c.y-c.x*m;
		double invm = 1d/m;
		double invb = c.x-c.y*invm;
		for(int i = 0; i < forces.size(); i++) {
			Force f = forces.get(i);
			if(f instanceof Spring) {
				Spring s = (Spring)f;
				greater.x = s.ends[0].pos.x>s.ends[1].pos.x?s.ends[0].pos.x:s.ends[1].pos.x;
				lesser.x = s.ends[0].pos.x<s.ends[1].pos.x?s.ends[0].pos.x:s.ends[1].pos.x;
				greater.y = s.ends[0].pos.y>s.ends[1].pos.y?s.ends[0].pos.y:s.ends[1].pos.y;
				lesser.y = s.ends[0].pos.y<s.ends[1].pos.y?s.ends[0].pos.y:s.ends[1].pos.y;
				if(!(greater.x < c.x && greater.x < v.x) &&
						!(lesser.x > c.x && lesser.x > v.x) &&
						!(greater.y < c.y && greater.y < v.y) &&
						!(lesser.y > c.y && lesser.y > v.y)) {
					double sm = (double)(s.ends[0].pos.y-s.ends[1].pos.y)/(s.ends[0].pos.x-s.ends[1].pos.x);
					if(sm > 1) {
						double sb = s.ends[0].pos.y-sm*s.ends[0].pos.x;
						float x = (float) ((sb-b)/(m-sm));
						float greaterCx = c.x>v.x?c.x:v.x;
						float lesserCx = c.x<=v.x?c.x:v.x;
						if(x < greaterCx && x > lesserCx) {
							forces.remove(s);
							i--;
						}
					}else {
						double invsm = 1d/sm;
						double invsb = s.ends[0].pos.x-invsm*s.ends[0].pos.y;
						float y = (float) ((invsb-invb)/(invm-invsm));
						float greaterCy = c.y>v.y?c.y:v.y;
						float lesserCy = c.y<=v.y?c.y:v.y;
						if(y < greaterCy && y > lesserCy) {
							forces.remove(s);
							i--;
						}
					}
				}else{
//					System.out.println("optimized away");
				}
			}
		}
	}
	void addParticle(Particle p) {
		particles.add(p);
		if(p instanceof ObjectiveParticle) {
			objectiveparticles.add((ObjectiveParticle) p);
		}
	}
	public ArrayList<ObjectiveBox> getBoxes() {
		return boxes;
	}
	public void setBoxes(ArrayList<ObjectiveBox> boxes) {
		this.boxes = boxes;
	}
	public void addWall(Wall wall) {
		walls.add(wall);
	}
	public void addBox(ObjectiveBox objectiveBox) {
		boxes.add(objectiveBox);
	}
	public void addForce(Force f) {
		forces.add(f);
	}
	public void generateSquareGrid(float xoffset, float yoffset, float sideLengthXT, float sideLengthYT, float objectiveIndex) {
		int sideLengthX = (int)sideLengthXT;
		int sideLengthY = (int)sideLengthYT;
		Particle[][] particleMatrix = new Particle[sideLengthX][sideLengthY];
		for(int a = 0; a < sideLengthX; a++) {
			for(int b = 0; b < sideLengthY; b++) {
				if(b*sideLengthX+a!=(int)objectiveIndex) {
					addParticle(new Particle(new Vec2(50*a+xoffset, 50*b+yoffset), this));
				}else{
					addParticle(new ObjectiveParticle(new Vec2(50*a+xoffset, 50*b+yoffset), this));
				}
				particleMatrix[a][b] = getParticles().get(getParticles().size()-1);
				if(a!=0) {
					forces.add(new Spring(particleMatrix[a-1][b], particleMatrix[a][b])); // add spring to one to left
				}
				if(b!=0) {
					forces.add(new Spring(particleMatrix[a][b-1], particleMatrix[a][b])); // add spring to one above
				}else{
					particleMatrix[a][b].setLocked(true);
				}
			}
		}
		particleMatrix[sideLengthX/2][sideLengthY/2].vel.y+=0.05; // enough to perturb the mesh so no strange number are commonplace
		particleMatrix[sideLengthX/2][sideLengthY/2+1].vel.x+=0.05; // " "
	}
	public void generateWebGrid(float x, float y, float radius, float objectiveIndex) {
		int currentIndex = 0;
		addParticle(objectiveIndex!=currentIndex?new Particle(new Vec2(x, y), this):new ObjectiveParticle(new Vec2(x, y), this));
		int startingIndex = particles.size()-1;
		currentIndex++;
		int ringLength = 2;
		for(int r = 1; r < radius; r++) {
			int currInnerIndex = startingIndex-ringLength;
			if(r==1)currInnerIndex = startingIndex;
			ringLength *=2;
			System.out.println(ringLength);
			for(int i = 0; i < ringLength; i++) {
				float tx = x+(float) (r*75*Math.cos(Math.PI*2*(((float)i)/ringLength)));
				float ty = y+(float) (r*75*Math.sin(Math.PI*2*(((float)i)/ringLength)));
				addParticle(objectiveIndex!=currentIndex?new Particle(new Vec2(tx, ty), this):new ObjectiveParticle(new Vec2(tx, ty), this));
				currentIndex++;
				if(i!=0)forces.add(new Spring(particles.get(particles.size()-1), particles.get(particles.size()-2)));
				forces.add(new Spring(particles.get(particles.size()-1), particles.get(currInnerIndex)));
				if(r!=1&&i<ringLength-2)forces.add(new Spring(particles.get(particles.size()-1), particles.get(currInnerIndex+1)));
				if(r!=1&&i>=ringLength-2)forces.add(new Spring(particles.get(particles.size()-1), particles.get(startingIndex-ringLength/2+1)));
				if(i%2==0&&r!=1)currInnerIndex++;
				if(i==ringLength-1)forces.add(new Spring(particles.get(particles.size()-1), particles.get(startingIndex+1)));
				if(r==radius-1)particles.get(particles.size()-1).setLocked(true);
			}
			startingIndex = particles.size()-1;
		}
	}
}
