package com.groomble.physicsmeshgame.core; 

public class Vec2 {
	public float x;
	public float y;
	public Vec2() {
		x = 0;
		y = 0;
	}
	Vec2(float tx, float ty) {
		x = tx;
		y = ty;
	}
	void add(Vec2 a) {
		x+=a.x;
		y+=a.y;
	}
	public void setAsDiff(Vec2 a, Vec2 b) {
		x = a.x-b.x;
		y = a.y-b.y;
	}
	public void normalize() {
		float k = mag();
		x/=k;
		y/=k;
	}
	public void mult(float m) {
		x*=m;
		y*=m;
	}
	public void set(Vec2 t) {
		x = t.x;
		y = t.y;
	}
	public void sub(Vec2 s) {
		x -= s.x;
		y -= s.y;
	}
	public float dot(Vec2 b) {
		return x*b.x+y*b.y;
	}
	public float mag() {
		return (float)java.lang.Math.hypot(x, y);
	}
	public void set(float tx, float ty) {
		x = tx;
		y = ty;
	}
	public void zero() {
		x=0;
		y=0;
	}
}
