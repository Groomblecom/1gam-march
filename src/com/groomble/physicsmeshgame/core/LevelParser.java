package com.groomble.physicsmeshgame.core;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.ArrayList;

import com.groomble.physicsmeshgame.Start;
import com.groomble.physicsmeshgame.render.SwingRenderer;

public class LevelParser {
	private static String[] prefixes = {"wall", "box", "constantdirectionalforce", "squaregrid", "webgrid", "flipspider"}; // these need to be lower-cased, but in the file anything goes
	private static ArrayList<Float> helperList = new ArrayList<Float>();

	public static void parse(BufferedReader in, Game mgame) {
		try {
			while(true) { // loop until finished with in
				String inline = in.readLine();
				if(inline == null)break; // break when file is finished
				inline.trim();
				inline.toLowerCase();
				if(inline.length() == 0)continue;
				int prefixNum = -1;
				for(int i = 0; i < prefixes.length;i++) {
					if(inline.startsWith(prefixes[i])) {
						prefixNum = i;
						break;
					}
				}
				float[] parsed = {};
				if(prefixNum!=-1) {
					inline = inline.substring(inline.indexOf(':')+1);
					parsed = parseNumbers(inline);
				}
				switch(prefixNum) {
				case 0:mgame.addWall(new Wall(new Vec2(parsed[0], parsed[1]), new Vec2(parsed[2], parsed[3])));break; // two end vectors
				case 1:mgame.addBox(new ObjectiveBox(new Vec2(parsed[0], parsed[1]), new Vec2(parsed[2], parsed[3])));break; // pos, extent vectors
				case 2:mgame.addForce(new ConstantDirectionalForce(new Vec2(parsed[0], parsed[1]), mgame));break; // acceleration vector
				case 3:mgame.generateSquareGrid(parsed[0], parsed[1], parsed[2], parsed[3], parsed[4]);break; // pos vector, extent in particles, index of objective or -1 for none
				case 4:mgame.generateWebGrid(parsed[0], parsed[1], parsed[2], parsed[3]);break; // pos vector, radius in particles, and objectiveParticle index.
//				case 5:Start.r.rotateImage(1, Math.PI/2);break;
				case 5:((SwingRenderer)(Start.r)).imageFlipped[1]=true;break;
				default:System.out.println("Bad prefix: "+inline);break;
				}
			}
			in.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
	}

	private static float[] parseNumbers(String inline) {
		boolean isNum = false;
		String numHolder = "";
		while(inline.length() > 0) {
			char c = inline.charAt(0);
			if(!Character.isDigit(c) && c != '.' && c != '-') {
				if(isNum) {
					helperList.add(Float.valueOf(numHolder));
				}
				numHolder = "";
				isNum = false;
				inline = inline.substring(1);
				continue;
			}
			isNum = true;
			numHolder+=c;
			inline = inline.substring(1);
		}
		float[] ret = new float[helperList.size()];
		for(int i = 0; i < helperList.size(); i++) {
			ret[i] = helperList.get(i).floatValue();
		}
		helperList.clear();
		return ret;
	}
}
