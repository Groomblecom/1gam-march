package com.groomble.physicsmeshgame.core;

public class ObjectiveParticle extends Particle {
	private boolean completed = false;
	public ObjectiveParticle(Vec2 tpos, Game tmgame) {
		super(tpos, tmgame);
		mgame.totalObjectives++;
	}

	public boolean isCompleted() {
		return completed;
	}

	public void setCompleted(boolean completed) {
		if(completed&&!this.completed)mgame.completedCount++;
		if(!completed&&this.completed)mgame.completedCount--;
		this.completed = completed;
	}
	

}
