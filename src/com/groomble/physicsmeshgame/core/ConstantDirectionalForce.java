package com.groomble.physicsmeshgame.core;

public class ConstantDirectionalForce implements Force {
	Game g;
	Vec2 force;
	ConstantDirectionalForce(Vec2 tforce, Game tg) {
		force = tforce;
		g = tg;
	}
	@Override
	public void apply() {
		for(Particle a:g.getParticles()) {
			a.vel.add(force);
		}
	}

}
