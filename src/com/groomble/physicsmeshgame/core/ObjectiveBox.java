package com.groomble.physicsmeshgame.core;

public class ObjectiveBox {
	private Vec2 pos;
	private Vec2 extent;
	public ObjectiveBox(Vec2 tpos, Vec2 textent) {
		setPos(tpos);
		extent = textent;
	}
	void test(ObjectiveParticle p) {
		if(p.pos.x > getPos().x && p.pos.x < getPos().x+getExtent().x) {
			if(p.pos.y > getPos().y && p.pos.y < getPos().y+getExtent().y) {
				p.setCompleted(true);
			}
		}
	}
	public Vec2 getExtent() {
		return extent;
	}
	public void setExtent(Vec2 extent) {
		this.extent = extent;
	}
	public Vec2 getPos() {
		return pos;
	}
	public void setPos(Vec2 pos) {
		this.pos = pos;
	}
}
