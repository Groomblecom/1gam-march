package com.groomble.physicsmeshgame.core;

public interface Force {
	void apply();
}
