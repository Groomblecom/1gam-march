package com.groomble.physicsmeshgame.core;

public class Spring implements Force {
	public Particle[] ends = new Particle[2];
	protected float natlen = 0;
	private Vec2 offset = new Vec2();
	private Vec2 force = new Vec2();
	private float strength = 0.1f; // keep this value small or else springs will throw things into infinity
	private float damping = 0.0002f;
	public Spring(Particle end1, Particle end2) {
		ends[0] = end1;
		ends[1] = end2;
		offset.setAsDiff(end1.pos, end2.pos);
		natlen = (float) java.lang.Math.hypot(offset.x, offset.y);
	}

	@Override
	public void apply() {
		if(Float.isNaN(ends[1].pos.x) || Float.isNaN(ends[1].pos.y))System.exit(0);
		offset.setAsDiff(ends[0].pos, ends[1].pos);
		force.set(offset);
		force.normalize();
		float coefficient = -strength*(offset.mag()-natlen);
		force.mult(coefficient);
		//adding damping constant
		float dampingmult = ends[0].vel.mag();
		float tmag = ends[1].vel.mag();
		if(dampingmult < tmag)dampingmult=tmag;
		dampingmult*=damping;
		if(dampingmult > 0.5f)dampingmult=0.5f;
		force.mult(1-damping*dampingmult);
		ends[0].vel.add(force);
		force.mult(-1);
		ends[1].vel.add(force);
	}
}
