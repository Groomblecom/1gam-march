package com.groomble.physicsmeshgame;

import java.awt.Color;
import java.awt.Desktop;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.UIManager;

import com.groomble.physicsmeshgame.core.Game;
import com.groomble.physicsmeshgame.core.LevelParser;
import com.groomble.physicsmeshgame.render.Renderer;
import com.groomble.physicsmeshgame.render.SwingRenderer;

public class Start {
	public static final int NUM_LEVELS = 8;
	static String levelPath = "/com/groomble/physicsmeshgame/assets/levels/level";
	static String levelSuffix = ".txt";
	static Game g;
	public static Renderer r;
	static JFrame f;
	static JComboBox<String> levelChooser;
	public static void main(String args[]) {
		f = new JFrame("PhysicsMeshDemo");
		f.setSize(600, 400);
		JPanel p = new JPanel();
		f.add(p);
		p.setLayout(new GridLayout(8, 1));
		p.add(new JLabel("Grooblecom's 1GAM Project March 2015"));
		p.add(new JLabel("Click and drag to cut webs."));
		JPanel startP = new JPanel();
		startP.setBackground(new Color(141, 184, 99));
		JButton start = new JButton("Start Game");
		start.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				startGame();
			}
		});
		startP.add(start);
		String[] levelNames = new String[NUM_LEVELS];
		for(int i = 0; i < NUM_LEVELS; i++) {
			levelNames[i] = "Level "+(i+1);
		}
		levelChooser = new JComboBox<String>(levelNames);
		startP.add(levelChooser);
		p.add(startP);
		JPanel linkP = new JPanel();
		linkP.setBackground(new Color(141, 184, 99));
		JButton website = new JButton("groomble.com website");
		website.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				try {
			        Desktop.getDesktop().browse(new URL("http://groomble.com").toURI());
			    } catch (Exception e1) {}
			}
		});
		linkP.add(website);
		p.add(linkP);
		f.setVisible(true);
		try {
			UIManager.setLookAndFeel("javax.swing.plaf.nimbus.NimbusLookAndFeel");
		}catch(Exception e) {}
		linkP.repaint();
	}
	public static void reset() {
		r.reset();
		g = new Game();
		LevelParser.parse(new BufferedReader(new InputStreamReader(Start.class.getResourceAsStream(levelPath+levelChooser.getSelectedIndex()+levelSuffix))), g);
		g.init(r);
	}
	public static void menu() {
		r.reset();
		f.setVisible(true);
	}
	static void startGame() {
		f.setVisible(false);
		System.out.println("starting");
		g = new Game();
		r = new SwingRenderer(g);
		LevelParser.parse(new BufferedReader(new InputStreamReader(Start.class.getResourceAsStream(levelPath+levelChooser.getSelectedIndex()+levelSuffix))), g);
		g.init(r);
	}
	public static void setLevel(int level) {
		levelChooser.setSelectedIndex(level);
	}
	public static int getLevel() {
		return levelChooser.getSelectedIndex();
	}
}
