package com.groomble.physicsmeshgame.render;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.RenderingHints;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionListener;
import java.awt.geom.AffineTransform;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.InputStream;

import javax.imageio.ImageIO;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.Timer;
import javax.swing.UIManager;
import javax.swing.JLayeredPane;

import com.groomble.physicsmeshgame.Start;
import com.groomble.physicsmeshgame.core.Force;
import com.groomble.physicsmeshgame.core.Game;
import com.groomble.physicsmeshgame.core.ObjectiveBox;
import com.groomble.physicsmeshgame.core.Particle;
import com.groomble.physicsmeshgame.core.Spring;
import com.groomble.physicsmeshgame.core.Vec2;
import com.groomble.physicsmeshgame.core.ObjectiveParticle;
import com.groomble.physicsmeshgame.core.Wall;
import com.groomble.physicsmeshgame.render.Renderer;

@SuppressWarnings("serial")
public class SwingRenderer extends JPanel implements Renderer, ActionListener, KeyListener, MouseMotionListener {
	private static final Color NODE_COLOR = new Color(0, 0, 0);
	private static final Color SPRING_COLOR = new Color(200, 200, 200);
	private static final BasicStroke SPRING_STROKE = new BasicStroke(3);
	private static final BasicStroke NORMAL_STROKE = new BasicStroke(1);
	private static final Color OBJECTIVE_BOX_COLOR = new Color(153, 153, 255, 153);
	private static final Color VICTORY_COLOR = new Color(0, 0, 255);
	private static final Color MENU_BACKGROUND = new Color(0, 0, 0, 150);
	private static final Color BACKGROUND_COLOR = new Color(255, 255, 255);
	private static final Color WALL_COLOR = new Color(0, 0, 0);
	
	String[] imageNames = {"fly.png", "spider.png", "branch.png"};
	int[][] imageDimensions = {{70, 70}, {200, 200}, {20, 20}};
	BufferedImage[] images = new BufferedImage[imageNames.length];
	
	JFrame window;
	JLayeredPane mainpane;
	JPanel menupanel;
	Timer repaintTimer;
	int wwidth = 1000, wheight = 1000;
	JButton resumeButton;
	JButton pauseButton;
	
	Game mgame;

	private boolean ready;
	private boolean imagesReady;
	private boolean started;
	private boolean paused;
	
	Vec2 pmouse;
	private Vec2 mouse;
	private int skipcount;
	private boolean won;
	private static int skipnum = 4;
	private AffineTransform translateHelper = new AffineTransform();
	public boolean[] imageFlipped = new boolean[imageNames.length];
	private String[] winStrings = {"Yum!", "A snack!", "Food!", "Tasty!", "Crunchy!"};
	public class ImageLoader extends Thread {
		@Override
		public void run() {
			for(int i = 0; i < images.length; i++) {
				InputStream in = getClass().getResourceAsStream("/com/groomble/physicsmeshgame/assets/images/"+imageNames[i]);
				try {
					images[i] = ImageIO.read(in);
					in.close();
					if(images[i].getWidth() == imageDimensions[i][0] && images[i].getHeight() == imageDimensions[i][1])continue;
					BufferedImage timage = new BufferedImage(imageDimensions[i][0], imageDimensions[i][1], BufferedImage.TYPE_4BYTE_ABGR);
					Graphics2D g = timage.createGraphics();
					g.drawImage(images[i], 0, 0, imageDimensions[i][0], imageDimensions[i][1], null);
					g.dispose();
					images[i] = timage;
					if(imageFlipped[i])rotateImage(i, Math.PI/2);
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			imagesReady=true;
			if(ready && !started) {
				repaintTimer.start();
				started = true;
			}
		}
	}
	public SwingRenderer(Game g) {
		super();
		new ImageLoader().start();
		mgame = g;
		window = new JFrame("Physics Mesh Game");
		window.setSize(wwidth, wheight);
		window.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		window.addComponentListener(new ComponentListener() {
			@Override
			public void componentHidden(ComponentEvent arg0) {}
			@Override
			public void componentMoved(ComponentEvent arg0) {}
			@Override
			public void componentResized(ComponentEvent arg0) {
				wwidth = window.getWidth();
				wheight = window.getHeight();
				resizeAll();
			}
			@Override
			public void componentShown(ComponentEvent arg0) {}
		});
		mainpane = new JLayeredPane();
		mainpane.setSize(wwidth, wheight);
		setBackground(BACKGROUND_COLOR);
		setLocation(0, 0);
		setSize(wwidth, wheight);
		mainpane.add(this, JLayeredPane.DEFAULT_LAYER);
		menupanel = new JPanel(){
			{
				setOpaque(false);
			}
			public void paintComponent(Graphics g) {
		        g.setColor(getBackground());
		        Rectangle r = g.getClipBounds();
		        g.fillRect(r.x, r.y, r.width, r.height);
		        super.paintComponent(g);
		    }
		};
		JButton restart = new JButton("Reset World");
		menupanel.add(restart);
		restart.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				Start.reset();
				mainpane.remove(mainpane.getIndexOf(menupanel));
			}
		});
		JButton menu = new JButton("Main Menu");
		menupanel.add(menu);
		menu.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				window.setVisible(false);
				Start.menu();
			}
		});
		resumeButton = new JButton("Resume Game");
		menupanel.add(resumeButton);
		resumeButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				mainpane.remove(mainpane.getIndexOf(menupanel));
				setPaused(false);
			}
		});
		//done adding things to menupanel
		menupanel.setBackground(MENU_BACKGROUND);
//		menupanel.setOpaque(false);
		menupanel.setLocation(0, 0);
		menupanel.setSize(wwidth, wheight);
		pauseButton = new JButton("||");
		pauseButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				setPaused(true);
				mainpane.add(menupanel, JLayeredPane.MODAL_LAYER);
				requestFocus();
			}
		});
		pauseButton.setLocation(wwidth-50, 50);
		pauseButton.setSize(pauseButton.getPreferredSize());
		mainpane.add(pauseButton, JLayeredPane.PALETTE_LAYER);
//		mainpane.add(menupanel, JLayeredPane.MODAL_LAYER);
		window.add(mainpane);
		window.setVisible(true);
		repaintTimer = new Timer(33, this);
		repaintTimer.setRepeats(true);
		addKeyListener(this);
		addMouseMotionListener(this);
		this.setFocusable(true);
		this.requestFocus();
		try {
		    UIManager.setLookAndFeel("javax.swing.plaf.nimbus.NimbusLookAndFeel");
		  }catch(Exception e) {}
	}

	protected void resizeAll() {
		setSize(wwidth, wheight);
		menupanel.setSize(wwidth, wheight);
		pauseButton.setLocation(wwidth-50, 50);
		pauseButton.setSize(pauseButton.getPreferredSize());
	}

	@Override
	public void begin(Game g) {
		mgame = g;
		mouse = new Vec2();
		pmouse = new Vec2();
		System.out.println("renderer starting");
		ready = true;
		if(imagesReady) {
			repaintTimer.start(); // if this doesn't start it, imageLoader will.
			started = true;
		}
	}

	@Override
	public void showMenu() {
		setPaused(true);
	}
	@Override
	public void actionPerformed(ActionEvent arg0) {
		mgame.step();
		repaint();
	}
	@Override
	protected void paintComponent(Graphics tg) {
		super.paintComponent(tg);
		Graphics2D g = (Graphics2D) tg;
		g.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
		if(!started) {
			tg.setFont(new Font("Bitstream Charter Serif", Font.PLAIN, 90));
			tg.drawString("Loading...", 100, 100);
			return;
		}
		g.setColor(SPRING_COLOR);
		g.setStroke(SPRING_STROKE);
		for(Force s:mgame.getForces()) {
			if(s instanceof Spring) {
				Spring t = (Spring)s;
				int cx = (int)(t.ends[0].pos.x+(int)t.ends[1].pos.x)/2;
				int cy = (int)(t.ends[0].pos.y+(int)t.ends[1].pos.y)/2;
				g.drawLine((int)t.ends[0].pos.x-2, (int)t.ends[0].pos.y-1, cx, cy);
				g.drawLine((int)t.ends[0].pos.x+2, (int)t.ends[0].pos.y+1, cx, cy);
				g.drawLine((int)t.ends[1].pos.x-2, (int)t.ends[1].pos.y-1, cx, cy);
				g.drawLine((int)t.ends[1].pos.x+2, (int)t.ends[1].pos.y+1, cx, cy);
			}
		}
		g.setStroke(NORMAL_STROKE);
		for(Particle p:mgame.getParticles()){
			if(p instanceof ObjectiveParticle) {
				translateHelper.setToTranslation(p.pos.x-images[0].getWidth()/2, p.pos.y-images[0].getHeight()/2);
				g.drawImage(images[0], translateHelper, this);
			}else if(p.isLocked()){
				g.setColor(NODE_COLOR);
				translateHelper.setToTranslation(p.pos.x-images[2].getWidth()/2, p.pos.y-images[2].getHeight()/2);
				g.drawImage(images[2], translateHelper, this);
			}
		}
		g.setColor(OBJECTIVE_BOX_COLOR);
		for(ObjectiveBox b:mgame.getBoxes()) {
			g.fillRect((int)b.getPos().x, (int)b.getPos().y, (int)b.getExtent().x, (int)b.getExtent().y);
			translateHelper.setToTranslation((int)b.getPos().x, (int)b.getPos().y);
			g.drawImage(images[1], translateHelper, this);
		}
		g.setColor(WALL_COLOR);
		for(Wall w:mgame.walls) {
			g.drawLine((int)w.getEnd0X(), (int)w.getEnd0Y(), (int)w.getEnd1X(), (int)w.getEnd1Y());
		}
		if(won) {
			g.setColor(VICTORY_COLOR);
			g.setFont(new Font("Bitstream Charter Serif", Font.PLAIN, 90));
			g.drawString(winStrings[(int) (Math.random()*winStrings.length)], 100, 100);
			pauseButton.setEnabled(false);
			pauseButton.setVisible(false);
			if(Start.getLevel()+1 < Start.NUM_LEVELS)Start.setLevel(Start.getLevel()+1);
			Timer temp = new Timer(1000, new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent arg0) {
					Start.reset();
				}
			});
			temp.setRepeats(false);
			temp.start();
		}
	}

	@Override
	public void setPaused(boolean pausing) {
		if(pausing == paused)return;
		if(pausing) {
			repaintTimer.stop();
		}else{
			repaintTimer.start();
		}
		paused = pausing;
	}

	@Override
	public void keyPressed(KeyEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void keyReleased(KeyEvent arg0) {
		if(arg0.getKeyCode() == KeyEvent.VK_ESCAPE) {
			if(!paused) {
				pauseButton.doClick();
			}else {
				resumeButton.doClick();
			}
		}else if(arg0.getKeyChar() == 'Q' || (arg0.getKeyChar() == 'q' && paused)) {
			System.exit(0);
		}
	}

	@Override
	public void keyTyped(KeyEvent arg0) {
		if(arg0.getKeyChar() == ' ') {
			setPaused(!paused);
		}
	}

	@Override
	public void mouseDragged(MouseEvent arg0) {
		if(mouse == null)return;
		if(skipcount !=skipnum)skipcount++;
		if(skipcount !=skipnum)return;
		mouse.x = arg0.getX();
		mouse.y = arg0.getY();
		mgame.cutSprings(mouse, pmouse);
		pmouse.set(mouse);
		skipcount %= skipnum;
	}

	@Override
	public void mouseMoved(MouseEvent arg0) {
		if(!ready)return;
		pmouse.set(mouse);
		mouse.x = arg0.getX();
		mouse.y = arg0.getY();
		
	}

	@Override
	public void win() {
		won = true;
		repaint();
	}

	@Override
	public void reset() {
		repaintTimer.stop();
		ready = false;
		started = false;
		paused = false;
		won = false;
		pauseButton.setEnabled(true);
		pauseButton.setVisible(true);
		for(int i = 0; i < images.length; i++) {
			if(imageFlipped[i]){
				imageFlipped[i] = false;
				InputStream in = getClass().getResourceAsStream("/com/groomble/physicsmeshgame/assets/images/"+imageNames[i]);
				try {
					images[i] = ImageIO.read(in);
					in.close();
					if(images[i].getWidth() == imageDimensions[i][0] && images[i].getHeight() == imageDimensions[i][1])continue;
					BufferedImage timage = new BufferedImage(imageDimensions[i][0], imageDimensions[i][1], BufferedImage.TYPE_4BYTE_ABGR);
					Graphics2D g = timage.createGraphics();
					g.drawImage(images[i], 0, 0, imageDimensions[i][0], imageDimensions[i][1], null);
					g.dispose();
					images[i] = timage;
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}

	@Override
	public void rotateImage(int i, double radians) {
		System.out.println("Flipping image");
//		if(true)return;
		imageFlipped[i] = !imageFlipped[i];
		BufferedImage timage = new BufferedImage(imageDimensions[i][0], imageDimensions[i][1], BufferedImage.TYPE_4BYTE_ABGR);
		Graphics2D g = timage.createGraphics();
		translateHelper.setToIdentity();
		translateHelper.rotate(radians);
		translateHelper.translate(0,-imageDimensions[1][0]);
		g.drawImage(images[i], translateHelper, null);
		g.dispose();
		images[i] = timage;
	}
}
