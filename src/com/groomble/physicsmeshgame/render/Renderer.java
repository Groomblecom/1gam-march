package com.groomble.physicsmeshgame.render;

import com.groomble.physicsmeshgame.core.Game;

public interface Renderer { // must call Game.step()!
	void begin(Game g);
	void showMenu();
	void setPaused(boolean paused);
	void win();
	void reset();
	void rotateImage(int i, double radians);
}
